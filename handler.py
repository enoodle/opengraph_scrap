"""
handler
"""
import json
import uuid
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute,
    JSONAttribute,
)
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
import opengraph_parse
import boto3


class ByIDIndex(GlobalSecondaryIndex):
    """ by url id index """
    class Meta:
        index_name = "byIDIndex"
        projection = AllProjection()
        read_capacity_units = 1
        write_capacity_units = 1
    url_id = UnicodeAttribute(hash_key=True)


class UrlScrap(Model):
    class Meta:  # pylint: disable=too-few-public-methods
        """
        Meta class for Issue
        """

        table_name = "my-urls-table"
        region = "eu-west-1"

    url_name = UnicodeAttribute(hash_key=True)
    url_id = UnicodeAttribute()
    url_opengraph_tags = JSONAttribute(null=True)
    by_url_id = ByIDIndex()


def save_new_url(url_name):
    new_id = None
    found = False
    while not found:
        new_id = str(uuid.uuid4())
        if not list(UrlScrap.by_url_id.query(new_id)):
            found = True
    url_scrap = UrlScrap(url_name=url_name, url_id=new_id)
    url_scrap.save()
    lambda_client = boto3.client('lambda')
    lambda_client.invoke_async(
        FunctionName='opengraph-parser-dev-scrap',
        InvokeArgs=json.dumps(
            {
                "url": url_name
            }
        )
    )
    return url_scrap


def get_id(event, _context):
    """ return the internal id for given url """
    print(event)

    url_name = event['queryStringParameters']['url']
    try:
        url_scrap = UrlScrap.get(url_name)
    except UrlScrap.DoesNotExist:
        url_scrap = save_new_url(url_name)
    return {
        'statusCode': 200,
        'body': url_scrap.url_id,
    }


def scrap(event, _context):
    try:
        og_data = opengraph_parse.parse_page(event['url'])
        url_scrap = UrlScrap.get(event['url'])
        url_scrap.url_opengraph_tags = og_data
        url_scrap.save()
    except:
        pass


def get_scrap_data(event, _context):
    url_id = event['pathParameters']['url_id']
    try:
        url_scrap = next(UrlScrap.by_url_id.query(url_id))
    except StopIteration:
        return {
            'statusCode': '404',
            'body': f'id {url_id} not found'
        }
    if not url_scrap.url_opengraph_tags:
        return {
            'statusCode': '202',
            'body': 'pending'
        }
    return {
            'statusCode': '200',
            'body': json.dumps(url_scrap.url_opengraph_tags),
        }
