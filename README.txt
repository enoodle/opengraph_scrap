﻿1.  Description
   1. Build a web service that will scrape  Open graph tags. For any given URL.
   2. See http://ogp.me/ for definitions
   3. See https://developers.facebook.com/tools/debug/sharing/?q=http%3A%2F%2Fogp.me%2F for an example implementation

2. Requirements
   1. We have provided a server located at erez.freiberger.hiring.keywee.io. You can find connection details below.
   2. The server has a minimal OS install (Ubuntu 16.04) and nothing else.
   3. To login VIA SSH:
      * ssh -i scraper-machine.pem ubuntu@erez.freiberger.hiring.keywee.io  - pem file is attached to email
   4. The server should provide the following JSON API

      *** ADD OR GET CANONICAL URL ID ***
      1. Request
            - POST erez.freiberger.hiring.keywee.io/stories?url={some_url}
         Response
            - Some unique ID (String, Int, Bigint...) that represents the *canonical form* of the given url (each url should have a single matching ID in the system)
            - Note: the idea is to have an ID value for each unique url that your service deals with. This ID is your internal representation of that URL
            - Canonical form: You should determine the "rel=canonical" form of the URL. Conversely, you can choose to use the og:url tag if it is available.
            - If no canonical form of the URL is available, then you should fallback to the input URL value.

      *** SCRAPE URL METADATA + IMAGES ***
      2. Request
            - GET erez.freiberger.hiring.keywee.io/stories/{url-unique-id}
            - Given the unique id received in the previous request, this endpoint will return the URL's metadata.
            - The metadata should be scraped only once, and then stored for future reference
            - While scraping the metadata for a URL, calling the endpoint again should return a "pending" status for that specific URL until scraping is completed
         Response
            {
               "id": "10150237978467733",
               "url": "http://ogp.me/",
               "type": "website",
               "title": "Open Graph protocol",
               "images": [
                  {
                     "url": "http://ogp.me/logo.png",
                     "type": "image/png",
                     "width": 300,
                     "height": 300,
                     "alt": "The Open Graph logo"
                  }
               ],
               "updated_time": "2018-02-18T03:41:09+0000",
               "scrape_status": "done"
            }
            - scrape_status field can be either (done, error, pending)
               - Done: means the service has already scrapped this URL and has completed successfully.
               - Pending: the service is currently scrapping this URL and the metadata is not ready yet.
               - Error: the service had previously attempted to scrape the URL and failed.

3. Notes
  1.  Use any stack or infrastructure you would like
  2.  Treat any response status other than 200 as an error
4. Please supply curl examples which demonstrate the usage of the service (IMPORTANT!!!!).
5. Please post your source code to a git repo or send it as a zip file.
